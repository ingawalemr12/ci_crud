<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function index()
	{
		$this->load->model('Usermodel');	//load model
		$users = $this->Usermodel->all();  // "select * from users"
		$data=array();
		$data['users'] = $users;
		$this->load->view('list', $data); //load data from model(select * from users), list(display page)
	}

	public function create()
	{
	$this->load->model('Usermodel');    //load model
		//form_validation here
	$this->form_validation->set_rules('name','Name','required|trim');
	$this->form_validation->set_rules('mail','Email','required|valid_email');
		
		if ($this->form_validation->run()==false) 
		{
			$this->load->view('create');
		}
		else
		{
			//echo "form success";
			$data=array();
			$data['Name']=$this->input->post('name');  //post data
			$data['Email']=$this->input->post('mail');
			$data['Created_date']= date('Y-m-d');

			$this->Usermodel->create($data);
			$this->session->set_flashdata('success','record added successfully');
			redirect(base_url().'index.php/User/index'); //post data and redirect to index method to fetch data
		}
	}

	function edit($userid)
	{
			$this->load->model('Usermodel');	//load model
	  $user=$this->Usermodel->getuser($userid);
			$data=array();
			$data['user'] = $user;

			$this->form_validation->set_rules('name','Name','required|trim'); //form_validation here
			$this->form_validation->set_rules('mail','Email','required|valid_email');

			if ($this->form_validation->run()==false) 
			{
				$this->load->view('edit', $data);
			}
			else
			{ //post data here and update user record

				$formdata=array();
				$formdata['Name'] =$this->input->post('name');
				$formdata['Email'] =$this->input->post('mail');

				$this->Usermodel->updateuser($userid, $formdata );
				$this->session->set_flashdata('success','record updated successfully');
				redirect(base_url().'index.php/User/index');
			}
			
	}
	
	function delete($userid)
	{
		$this->load->model('Usermodel');	//load model
		$this->Usermodel->deleteuser($userid);
		$this->session->set_flashdata('success','record deleted successfully');
		redirect(base_url().'index.php/User/index');
	}
}
