<!DOCTYPE html>
<html>
<head>
	<title>CRUD - Update User</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>

<body>
<div class="navbar navbar-dark bg-dark">
	<div class="container">
		<a href="#" class="navbar-brand">CRUD Application</a>
    </div>
</div>

<div class="container" style="padding-top: 10px;">
	<h3>Update User</h3>
	<hr>
	<form name="createUser" action="<?php echo base_url().'index.php/User/edit/'.$user['id']; ?>" 
		method="post">	
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="form-gorup">
	    			<label>Name</label>
	    			<input type="text" name="name" value="<?php echo set_value('name',  $user['Name']) ?>" class="form-control">
	    			<?php echo form_error('name'); ?>
	    		</div>
	    		<div class="form-gorup">
	    			<label>Email</label>
	    			<input type="text" name="mail" value="<?php echo set_value('mail',  $user['Email']) ?>" class="form-control"><br>
	    			<?php echo form_error('mail'); ?>
	    		</div>
	    		<div class="form-gorup">
	    			<button class="btn btn-primary">Update</button>
	    			<a href="<?php echo base_url().'index.php/user/index'; ?>" class="btn btn-secondary ">Cancel</a>
	    		</div>
	    	</div>
	    </div>
    </form>
</div>
</body>

</html>