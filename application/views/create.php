<!DOCTYPE html>
<html>
<head>
	<title>CRUD - Create User</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>

<body>
<div class="navbar navbar-dark bg-dark">
	<div class="container">
		<a href="#" class="navbar-brand">CRUD Application</a>
    </div>
</div>

<div class="container" style="padding-top: 10px;">
	<h3>Create User</h3>
	<hr>
	<form name="createUser" action="index.php/User/create" method="post">	
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="form-gorup">
	    			<label>Name</label>
	    			<input type="text" name="name" value="<?php echo set_value('name') ?>" class="form-control">
	    			<?php echo form_error('name'); ?>
	    		</div>
	    		<div class="form-gorup">
	    			<label>Email</label>
	    			<input type="text" name="mail" value="<?php echo set_value('mail') ?>" class="form-control"><br>
	    			<?php echo form_error('mail'); ?>
	    		</div>
	    		<div class="form-gorup">
	    			<button class="btn btn-primary">Create</button>
	    			<a href="<?php echo base_url().'index.php/user/index'; ?>" class="btn btn-secondary ">Cancel</a>
	    		</div>
	    	</div>
	    </div>
    </form>
</div>
</body>

</html>