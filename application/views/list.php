<!DOCTYPE html>
<html>
<head>
	<title>CRUD - List User</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>

<body>
<div class="navbar navbar-dark bg-dark">
	<div class="container">
		<a href="#" class="navbar-brand">CRUD Application</a>
    </div>
</div>

<div class="container" style="padding-top: 10px;">
	<div class="row">
		<div class="col-12">
		<?php
		$success = 	$this->session->userdata('success');
			if($success !="")
				{ ?> 
			<div class="alert alert-success"><?php echo $success; ?></div>	
		<?php	} ?>
        
        <?php
		$fail = $this->session->userdata('fail');
			if($fail !="")
				{ ?> 
			<div class="alert alert-success"><?php echo $fail; ?></div>	
		<?php	} ?>


		
       
		</div>
	</div>
	<div class="row">
		<div class="col-6"><h3>List of Users</h3></div>
		<div class="col-6">
		  <a href="<?php echo base_url().'index.php/User/create'; ?>" class="btn btn-primary">Create User</a>
		</div>
	</div> 
	<hr> 
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Email</th>
					<th>Date</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
				<?php if(!empty($users))
				{ 
					foreach($users as $value) 
					{ ?>
						<tr>
							<td><?php echo $value['id']; ?></td>
							<td><?php echo $value['Name']; ?></td>
							<td><?php echo $value['Email']; ?></td>
							<td><?php echo $value['Created_date']; ?></td>
							<td><a href="<?php echo base_url().'index.php/User/edit/'.$value['id'] ?>" class="btn btn-success">Edit</a></td>
							<td><a href="<?php echo base_url().'index.php/User/delete/'.$value['id'] ?>" class="btn btn-danger">Delete</a></td>
						</tr>
				<?php	
					} 
				} 
				else { ?>
					<span>No record Found</span>
				<?php }  ?>
			</table>
		</div>
	</div>
</div>
</body>

</html>