<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodel extends CI_model 
{
	public function create($data)
	{
		$this->db->insert('users', $data);  // "insert into users values(null,'$name','$email');"
	}

	function all()
	{
		   return $users = $this->db->get('users')->result_array(); // "select * from users"
	}

	function getuser($userid)
	{
		        $this->db->where('id', $userid); // where id='$userid';
return	$user =	$this->db->get('users')->row_array(); //  "select * from users where id='$userid'";
	}

	function updateuser($userid, $formdata)
	{
		//update users set Name='$name' where id='$userid';
		$this->db->where('id', $userid);		// where id='$userid';
		$this->db->update('users', $formdata);  //"update table set values($formdata)"
	}											//"update users set Name='$name';

	function deleteuser($userid)
	{
		$this->db->where('id', $userid); // where id= '?' ;
		$this->db->delete('users');  //delete from users
	}
}
